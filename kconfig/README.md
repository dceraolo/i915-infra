# i915 Infra Kernel Configs

## debug

Default kernel config for CI purposes with a lot of debug options turned on.

## debug-kasan

Same as debug kernel but with KASAN enabled.

## debug-gcov

Same as debug kernel but with gcov enabled.



